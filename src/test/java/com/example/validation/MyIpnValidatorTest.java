package com.example.validation;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class MyIpnValidatorTest {

    private final MyIpnValidator validator = new MyIpnValidator();

    @Test
    void testValidIpn() {
        assertTrue(validator.isValid("1234567899", null));
    }

    @Test
    void testInvalidIpn_Null() {
        assertFalse(validator.isValid(null, null));
    }

    @Test
    void testInvalidIpn_Blank() {
        assertFalse(validator.isValid("", null));
    }

    @Test
    void testInvalidIpn_WrongFormat() {
        assertFalse(validator.isValid("12345abcde", null));
    }

    @Test
    void testInvalidIpn_WrongLength() {
        assertFalse(validator.isValid("123456789", null));
    }

    @Test
    void testInvalidIpn_BirthYearTooEarly() {
        assertFalse(validator.isValid("1910012345", null));
    }

    @Test
    void testInvalidIpn_BirthYearTooLate() {
        assertFalse(validator.isValid("2200012345", null));
    }

    @Test
    void testInvalidIpn_AgeTooOld() {
        assertFalse(validator.isValid("0300012345", null));
    }

    @Test
    void testInvalidIpn_AgeTooYoung() {
        assertFalse(validator.isValid("1100012345", null));
    }

    @Test
    void testInvalidIpn_InvalidControlNumber() {
        assertFalse(validator.isValid("1234567891", null));
    }

    @Test
    void testInvalidIpn_BirthDateInFuture() {
        assertFalse(validator.isValid("9910012345", null));
    }
}
