package com.example.service;

import com.example.entity.UserEntity;
import com.example.model.UserDTO;
import com.example.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Test
     void testCreateUser() {
        UserRepository repositoryMock = mock(UserRepository.class);
        UserService userService = new UserService(repositoryMock);

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("John");
        userDTO.setLastName("Doe");
        userDTO.setIPn("1234567899");

        UserEntity expectedUserEntity = new UserEntity();
        expectedUserEntity.setFirstName("John");
        expectedUserEntity.setLastName("Doe");
        expectedUserEntity.setIPn("1234567899");

        when(repositoryMock.save(any(UserEntity.class))).thenReturn(expectedUserEntity);

        UserEntity createdUser = userService.create(userDTO);

        verify(repositoryMock, times(1)).save(any(UserEntity.class));
        assertEquals(expectedUserEntity.getFirstName(), createdUser.getFirstName());
        assertEquals(expectedUserEntity.getLastName(), createdUser.getLastName());
        assertEquals(expectedUserEntity.getIPn(), createdUser.getIPn());
    }

    @Test
     void testReadAllUsers() {
        UserRepository repositoryMock = mock(UserRepository.class);
        UserService userService = new UserService(repositoryMock);

        UserEntity user1 = new UserEntity();
        user1.setId(1L);
        user1.setFirstName("John");
        user1.setLastName("Doe");
        user1.setIPn("1234567890");

        UserEntity user2 = new UserEntity();
        user2.setId(2L);
        user2.setFirstName("Jane");
        user2.setLastName("Smith");
        user2.setIPn("9876543210");

        when(repositoryMock.findAll()).thenReturn(List.of(user1, user2));

        Iterable<UserEntity> users = userService.readAll();

        verify(repositoryMock, times(1)).findAll();
        assertEquals(2, ((List<UserEntity>) users).size());
    }

    @Test
     void testGetUserById() {
        UserRepository repositoryMock = mock(UserRepository.class);
        UserService userService = new UserService(repositoryMock);

        UserEntity expectedUser = new UserEntity();
        expectedUser.setId(1L);
        expectedUser.setFirstName("John");
        expectedUser.setLastName("Doe");
        expectedUser.setIPn("1234567899");

        when(repositoryMock.findById(1L)).thenReturn(Optional.of(expectedUser));

        UserEntity actualUser = userService.getById(1L);

        verify(repositoryMock, times(1)).findById(1L);
        assertEquals(expectedUser, actualUser);
    }

   @Test
   void testUpdateUser() {
      UserRepository repositoryMock = mock(UserRepository.class);
      UserService userService = new UserService(repositoryMock);

      Long userId = 1L;

      UserDTO updatedUserDTO = new UserDTO();
      updatedUserDTO.setFirstName("Updated");
      updatedUserDTO.setLastName("User");
      updatedUserDTO.setIPn("1234567899");

      UserEntity existingUser = new UserEntity();
      existingUser.setId(userId);
      existingUser.setFirstName("John");
      existingUser.setLastName("Doe");
      existingUser.setIPn("3227707737");

      when(repositoryMock.findById(userId)).thenReturn(Optional.of(existingUser));
      when(repositoryMock.save(existingUser)).thenReturn(existingUser);

      UserEntity updatedUser = userService.update(updatedUserDTO, userId);

      verify(repositoryMock, times(1)).findById(userId);
      verify(repositoryMock, times(1)).save(existingUser);

      assertEquals(updatedUserDTO.getFirstName(), updatedUser.getFirstName());
      assertEquals(updatedUserDTO.getLastName(), updatedUser.getLastName());
      assertEquals(updatedUserDTO.getIPn(), updatedUser.getIPn());
   }


   @Test
     void testDeleteUser() {
        UserRepository repositoryMock = mock(UserRepository.class);
        UserService userService = new UserService(repositoryMock);

        Long userId = 1L;

        UserEntity userToDelete = new UserEntity();
        userToDelete.setId(userId);
        userToDelete.setFirstName("John");
        userToDelete.setLastName("Doe");
        userToDelete.setIPn("1234567890");

        when(repositoryMock.findById(userId)).thenReturn(Optional.of(userToDelete));

        UserEntity deletedUser = userService.delete(userId);

        verify(repositoryMock, times(1)).findById(userId);
        verify(repositoryMock, times(1)).deleteById(userId);
        assertEquals(userToDelete, deletedUser);
    }
}
