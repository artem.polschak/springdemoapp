package com.example.controllers;

import com.example.exception.UserNotFoundException;
import com.example.service.UserService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@Disabled
@WebMvcTest(ErrorHandlingControllerAdvice.class)
public class ErrorHandlingControllerAdviceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    void testUserNotFoundExceptionHandling() throws Exception {
        doThrow(new UserNotFoundException("User not found")).when(userService).getById(any());

        ResultActions result = mockMvc.perform(get("/users/get/1"));

        result.andExpect(status().isNotFound())
                .andExpect(content().json("{\"message\":\"User not found\"}"));
    }

    @Test
    void testValidationExceptionHandler() throws Exception {
        String jsonRequest = "{\"firstName\": \"\", \"lastName\": \"Doe\", \"IPn\": \"1234567890\"}";

        ResultActions result = mockMvc.perform(post("/users/create")
                .contentType("application/json")
                .content(jsonRequest));

        result.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.firstName").value("First name cannot be empty"))
                .andExpect(jsonPath("$.lastName").doesNotExist())
                .andExpect(jsonPath("$.IPn").doesNotExist());
    }
}
