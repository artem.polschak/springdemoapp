package com.example.controllers;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.example.entity.UserEntity;
import com.example.model.UserDTO;
import com.example.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
@Disabled
@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private UserDTO userDTO;
    private UserEntity userEntity;

    @BeforeEach
    void setUp() {
        userDTO = new UserDTO();
        userDTO.setFirstName("John");
        userDTO.setLastName("Doe");
        userDTO.setIPn("1234567899");

        userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setFirstName("John");
        userEntity.setLastName("Doe");
        userEntity.setIPn("1234567899");
    }

    @Test
    void testReadAllUsers() throws Exception {
        when(userService.readAll()).thenReturn(Collections.singletonList(userEntity));

        mockMvc.perform(get("/users/getAll"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value("John"))
                .andExpect(jsonPath("$[0].lastName").value("Doe"))
                .andExpect(jsonPath("$[0].IPn").value("1234567899"));

        verify(userService, times(1)).readAll();
    }

    @Test
    void testCreateUser() throws Exception {
        when(userService.create(any(UserDTO.class))).thenReturn(userEntity);

        mockMvc.perform(post("/users/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.IPn").value("1234567899"));

        verify(userService, times(1)).create(any(UserDTO.class));
    }

    @Test
    void testGetUserById() throws Exception {
        when(userService.getById(1L)).thenReturn(userEntity);

        mockMvc.perform(get("/users/get/1"))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.IPn").value("1234567899"));

        verify(userService, times(1)).getById(1L);
    }

    @Test
    void testUpdateUser() throws Exception {
        when(userService.update(any(UserDTO.class), eq(1L))).thenReturn(userEntity);

        mockMvc.perform(put("/users/update/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.IPn").value("1234567899"));

        verify(userService, times(1)).update(any(UserDTO.class), eq(1L));
    }

    @Test
    void testDeleteUser() throws Exception {
        when(userService.delete(1L)).thenReturn(userEntity);

        mockMvc.perform(delete("/users/delete/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.IPn").value("1234567899"));

        verify(userService, times(1)).delete(1L);
    }

    // Helper method to convert object to JSON string
    private static String asJsonString(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
}
