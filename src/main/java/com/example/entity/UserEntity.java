package com.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;
    @Column(name = "Ipn")
    //@JsonIgnore
    private String iPn;

    public UserEntity() {

    }
    public UserEntity(String firstName, String lastName, String iPn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.iPn = iPn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



    public String getIPn() {
        return iPn;
    }

    public void setIPn(String iPn) {
        this.iPn = iPn;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof UserEntity))
            return false;
        UserEntity userEntity = (UserEntity) o;
        return Objects.equals(this.id, userEntity.id) && Objects.equals(this.firstName, userEntity.firstName)
                && Objects.equals(this.lastName, userEntity.lastName) && Objects.equals(this.iPn, userEntity.iPn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.firstName, this.lastName, this.iPn);
    }

    @Override
    public String toString() {
        return "User{" + "id=" + this.id + ", firstName='" + this.firstName + '\'' + ", lastName='" + this.lastName
                + '\'' + ", IPN='" + this.iPn + '\'' + '}';
    }
}
