package com.example.db;

import com.example.entity.UserEntity;
import com.example.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initUserDatabase(UserRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new UserEntity("Bilbo", "Baggins", "1234567899")));
            log.info("Preloading " + repository.save(new UserEntity("Frodo", "Baggins", "1234567899")));
            log.info("Preloading " + repository.save(new UserEntity("Bob", "Marley", "1234567899")));
            log.info("Preloading " + repository.save(new UserEntity("Sam", "Winchester", "1234567899")));
          /*  log.info("Preloading " + productRepository.save(new ProductEntity(new UserEntity("Bilbo", "Baggins", "1234567899")
                    , "Дима"
                    , "описание Димы", "Bob", "bob Description")));
            log.info("Preloading " + productRepository.save(new ProductEntity(new UserEntity("Bilbo", "Baggins", "1234567899")
                    , "Дима"
                    , "описание Димы", "Bob", "bob Description")));*/

        };
    }

}
