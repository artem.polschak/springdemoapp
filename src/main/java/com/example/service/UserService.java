package com.example.service;

import com.example.entity.UserEntity;
import com.example.exception.UserNotFoundException;
import com.example.model.UserDTO;
import com.example.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;


@Service
public class UserService {
    private static final Logger logger = LogManager.getLogger(UserService.class);
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public UserEntity create(UserDTO userDTO) {

            UserEntity userEntity = new UserEntity();
            userEntity.setFirstName(userDTO.getFirstName());
            userEntity.setLastName(userDTO.getLastName());
            userEntity.setIPn(userDTO.getIPn());
        logger.info("PersonRepository created {}", userDTO);
            return repository.save(userEntity);
    }

    public Iterable<UserEntity> readAll() {
        logger.info("PersonRepository readAll ");
        return repository.findAll();
    }

    public UserEntity getById(Long id) {
        logger.info("PersonRepository getById {}", id);
        return repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Not found Person by Id " + id));
    }


    public UserEntity update(UserDTO userDTO, Long id) {

        UserEntity userEntity = repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Person not found with id: " + id));

        userEntity.setFirstName(userDTO.getFirstName());
        userEntity.setLastName(userDTO.getLastName());
        userEntity.setIPn(userDTO.getIPn());
        logger.info("PersonRepository update {}, {}", userDTO, id);
        return repository.save(userEntity);
    }


    public UserEntity delete(Long id) {
        UserEntity userEntity = repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Person not found with id: " + id));

        repository.deleteById(id);
        logger.info("PersonRepository deleteById {}", id);
        return userEntity;
    }
}
