package com.example.controllers;

import com.example.entity.UserEntity;

import com.example.model.UserDTO;
import com.example.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

@RestController
@RequestMapping("/users")
@Validated
public class UserController {
    private static final Logger logger = LogManager.getLogger(UserController.class);
    private final UserService service;

    UserController(UserService service) {
        this.service = service;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Iterable<UserEntity>> readAll() {
        logger.info("readAll Controller working");
        return new ResponseEntity<>(service.readAll(), HttpStatus.OK);
    }


    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserEntity> create(@Valid @RequestBody UserDTO userDTO) {
        logger.info("create Controller working {}", userDTO);
        return new ResponseEntity<>(service.create(userDTO), HttpStatus.CREATED);

    }

    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getById(@PathVariable Long id) {
        logger.info("getById Controller working id {}", id);
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<UserEntity> update(@Valid @RequestBody UserDTO userDTO, @PathVariable Long id) {
        logger.info("update Controller working id {}", id);
        return new ResponseEntity<>(service.update(userDTO, id), HttpStatus.OK);
    }



    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<UserEntity> delete(@PathVariable Long id) {
        logger.info("delete Controller working id {}", id);
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }
}
