package com.example.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MyIpnValidator.class)
@Documented
public @interface IpnValidation {
    String message() default "Invalid Ipn";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

