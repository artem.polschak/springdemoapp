package com.example.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.regex.Pattern;

public class MyIpnValidator implements ConstraintValidator<IpnValidation, String> {
    private static final Logger logger = LogManager.getLogger(MyIpnValidator.class);
    private static final String IPN_REGEX = "\\d{10}";
    private static final int MIN_BIRTH_YEAR = 1923;
    private static final int MAX_AGE = 100;
    private static final int MIN_AGE = 1;


    @Override
    public boolean isValid(String ipn, ConstraintValidatorContext context) {
        logger.info("Validator ipn {}", ipn);
        if (ipn == null || ipn.isBlank()) {
            return false;
        }

        if (!isValidFormat(ipn)) {
            return false;
        }

        if (!isValidLength(ipn)) {
            return false;
        }

        LocalDate birthDate = extractBirthDate(ipn);
        if (!isValidBirthYear(birthDate)) {
            return false;
        }

        if (!isValidAge(birthDate)) {
            return false;
        }

        return isValidControlNumber(ipn);

    }

    private boolean isValidFormat(String ipn) {
        return Pattern.matches(IPN_REGEX, ipn);
    }

    private boolean isValidLength(String ipn) {
        return ipn.length() == 10;
    }

    private LocalDate extractBirthDate(String ipn) {
        int daysSince1899 = Integer.parseInt(ipn.substring(0, 5));
        return LocalDate.of(1899, 12, 31).plusDays(daysSince1899);
    }

    private boolean isValidBirthYear(LocalDate birthDate) {
        int birthYear = birthDate.getYear();
        int currentYear = LocalDate.now().getYear();
        return birthYear >= MIN_BIRTH_YEAR && birthYear <= currentYear;
    }

    private boolean isValidAge(LocalDate birthDate) {
        int age = LocalDate.now().getYear() - birthDate.getYear();
        return age >= MIN_AGE && age <= MAX_AGE;
    }

    private boolean isValidControlNumber(String ipn) {
        char[] ipnDigits = ipn.toCharArray();
        int[] coefficients = {-1, 5, 7, 9, 4, 6, 10, 5, 7};
        int sum = 0;

        for (int i = 0; i < 9; i++) {
            int digit = Character.getNumericValue(ipnDigits[i]);
            sum += coefficients[i] * digit;
        }

        int remainder = sum % 11;
        int calculatedControlNumber = (remainder == 10) ? 0 : remainder;

        int lastDigit = Character.getNumericValue(ipnDigits[9]);

        return calculatedControlNumber == lastDigit;
    }

}


