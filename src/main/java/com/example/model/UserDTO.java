package com.example.model;

import com.example.validation.IpnValidation;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

public class UserDTO {

    private String firstName;
    private String lastName;

    private String iPn;

    public UserDTO() {}

    public UserDTO(String firstName, String lastName, String iPn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.iPn = iPn;
    }
    @NotBlank(message = "First Name is required")
    @Size(min = 2, max = 40, message = "First Name must be between 2 and 40 letters")
    @Pattern(regexp = "^[a-zA-Zа-яА-Я]+$", message = "First Name must contain only letters")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @NotBlank(message = "Last Name is required")
    @Size(min = 2, max = 40, message = "Last Name must be between 2 and 40 letters")
    @Pattern(regexp = "^[a-zA-Zа-яА-Я]+$", message = "Last Name must contain only letters")
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @IpnValidation(message = "Invalid Ipn, It`s should contains only 10 valid digits")
    public String getIPn() {
        return iPn;
    }

    public void setIPn(String iPn) {
        this.iPn = iPn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO that = (UserDTO) o;
        return Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName) && Objects.equals(iPn, that.iPn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, iPn);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", IPN='" + iPn + '\'' +
                '}';
    }
}
